/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 15:56:04 by jwong             #+#    #+#             */
/*   Updated: 2018/04/26 18:35:39 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	set_offset(t_win *win, t_line *map, int win_dist)
{
	int		i;
	int		x;
	int		y;
	t_point	*p1;
	t_point	*p3;

	p1 = &map[0].data[0];
	i = 0;
	while (map[i + 1].data)
		i++;
	p3 = &map[i].data[0];
	if (win_dist == 1)
	{
		x = iso_point_x(p3, 0, win->dist, win->alt);
		y = iso_point_y(p1, 0, win->dist, win->alt);
	}
	else
	{
		x = iso_point_x(p3, 0, MAX_DIST, win->alt);
		y = iso_point_y(p1, 0, MAX_DIST, win->alt);
	}
	if (x < 0)
		win->offset_x = abs(x);
	if (y < 0)
		win->offset_y = abs(y);
}

void	scale_dist(t_win *win, int dx, int dy)
{
	win->x = MAX_WIN_LEN_X;
	win->y = MAX_WIN_LEN_Y;
	if (dx < dy)
		win->dist = (double)MAX_WIN_LEN_Y / dy * MAX_DIST;
	else
		win->dist = (double)MAX_WIN_LEN_X / dx * MAX_DIST;
	if (win->dist <= 0)
		win->dist = 1;
}

int		scale_window(t_win *win, int dx, int dy)
{
	if (dx <= MAX_WIN_LEN_X && dy <= MAX_WIN_LEN_Y)
	{
		if (abs(dx) > MAX_WIN_LEN_X || abs(dy) > MAX_WIN_LEN_Y)
			return (FALSE);
		win->x = abs(dx);
		win->y = abs(dy);
		win->dist = MAX_DIST;
		return (TRUE);
	}
	return (FALSE);
}

int		get_win_dimensions(t_win *win, t_line *map)
{
	t_point	*p1;
	t_point	*p2;
	t_point	*p3;
	t_point	*p4;
	int		i;
	int		dx;
	int		dy;

	i = 0;
	p1 = &map[0].data[0];
	p2 = &map[0].data[map[0].len - 1];
	while (map[i + 1].data)
		i++;
	p3 = &map[i].data[0];
	p4 = &map[i].data[map[i].len - 1];
	dx = iso_point_x(p2, 0, MAX_DIST, win->alt)
		- iso_point_x(p3, 0, MAX_DIST, win->alt);
	dy = iso_point_y(p4, 0, MAX_DIST, win->alt)
		- iso_point_y(p1, 0, MAX_DIST, win->alt);
	if (scale_window(win, dx, dy) == FALSE)
	{
		scale_dist(win, dx, dy);
		return (1);
	}
	return (0);
}
