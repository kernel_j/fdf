/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 14:11:47 by jwong             #+#    #+#             */
/*   Updated: 2018/04/26 18:28:29 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "fdf.h"

int	is_hex_char(char c)
{
	if (c == 'A' || c == 'a')
		return (TRUE);
	else if (c == 'B' || c == 'b')
		return (TRUE);
	else if (c == 'C' || c == 'c')
		return (TRUE);
	else if (c == 'D' || c == 'd')
		return (TRUE);
	else if (c == 'E' || c == 'e')
		return (TRUE);
	else if (c == 'F' || c == 'f')
		return (TRUE);
	return (FALSE);
}

int	is_hex(char **str)
{
	int i;

	i = 0;
	while (**str && **str != ' ')
	{
		if (i == 0 && **str != '0')
			return (FALSE);
		else if (i == 1 && **str != 'x')
			return (FALSE);
		else if (i > 1
				&& ft_isdigit(**str) != 1
				&& is_hex_char(**str) != 1)
			return (FALSE);
		i++;
		(*str)++;
	}
	return (TRUE);
}

int	is_num(char **str)
{
	while (**str && **str != ' ' && **str != ',')
	{
		if (ft_isdigit(**str) != 1)
			return (FALSE);
		(*str)++;
	}
	return (TRUE);
}

int	check_hex(char **line)
{
	(*line)++;
	if (is_hex(line) == FALSE)
		return (FALSE);
	return (TRUE);
}

int	check_num(char **line, int *elem)
{
	if (is_num(line) == FALSE)
		return (FALSE);
	else
	{
		(*elem)++;
	}
	return (TRUE);
}
