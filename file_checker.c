/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_checker.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/22 13:41:39 by jwong             #+#    #+#             */
/*   Updated: 2018/04/26 17:26:53 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include "libft.h"
#include "fdf.h"

int	check_elem(t_win *win, int elem, int *i)
{
	if (*i == 0 && win->x < elem)
		win->x = elem;
	else if (*i != 0 && win->x != elem)
		return (FALSE);
	return (TRUE);
}

int	is_valid(char *line, t_win *win, int *i)
{
	int elem;

	elem = 0;
	while (*line)
	{
		if (*line != ' ' && *line != '-')
		{
			if (*line == ',')
			{
				if (check_hex(&line) == FALSE)
					return (FALSE);
			}
			else
			{
				if (check_num(&line, &elem) == FALSE)
					return (FALSE);
			}
		}
		else
			line++;
	}
	if (check_elem(win, elem, i) == FALSE)
		return (FALSE);
	return (TRUE);
}

int	readfile(int fd, t_win *win)
{
	int		i;
	char	*buff;

	i = 0;
	buff = NULL;
	while (get_next_line(fd, &buff) > 0)
	{
		if (is_valid(buff, win, &i) != TRUE)
		{
			free(buff);
			close(fd);
			error_handler(FILE_ERR);
			return (FALSE);
		}
		i++;
		free(buff);
		win->y++;
	}
	close(fd);
	return (TRUE);
}

int	check_empty_file(char *file)
{
	int		fd;
	int		ret;
	char	buff[3];

	fd = open(file, O_RDONLY);
	if (fd == -1)
		error_handler(FD_ERR);
	if((ret = read(fd, buff, 3)) == -1)
		error_handler(FD_ERR);
	if (ret <= 2)
	{
        if (ret == 0)
		    error_handler(FILE_EMPTY);
        else if (ret == 1 || ret == 2)
            error_handler(FILE_ERR);
		close(fd);
		exit(-1);
	}
	return (FALSE);
}

int	file_checker(char *file, t_win *win)
{
	int		fd;
	size_t	ret;

	ret = check_empty_file(file);
	fd = open(file, O_RDONLY);
	if (fd == -1)
		error_handler(FD_ERR);
	ret = readfile(fd, win);
	return (ret);
}
