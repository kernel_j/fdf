/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colours.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/23 15:18:14 by jwong             #+#    #+#             */
/*   Updated: 2018/04/23 15:55:58 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "fdf.h"

void	blue_palette(t_point *pt)
{
	if (pt->z < 0)
		pt->colour = ft_strdup("0x66B2FF");
	else if (pt->z >= 0 && pt->z < 2)
		pt->colour = ft_strdup("0x3399FF");
	else if (pt->z >= 2 && pt->z < 4)
		pt->colour = ft_strdup("0x0080FF");
	else if (pt->z >= 4 && pt->z < 6)
		pt->colour = ft_strdup("0x0000FF");
	else if (pt->z >= 6 && pt->z < 8)
		pt->colour = ft_strdup("0x0000CC");
    else
		pt->colour = ft_strdup("0x000099");
}

void	red_palette(t_point *pt)
{
	if (pt->z < 0)
		pt->colour = ft_strdup("0xCC0000");
	else if (pt->z >= 0 && pt->z < 2)
		pt->colour = ft_strdup("0xFF0000");
	else if (pt->z >= 2 && pt->z < 4)
		pt->colour = ft_strdup("0xFF3333");
	else if (pt->z >= 4 && pt->z < 6)
		pt->colour = ft_strdup("0xFF6666");
	else if (pt->z >= 6 && pt->z < 8)
		pt->colour = ft_strdup("0xFF9999");
    else
		pt->colour = ft_strdup("0xFFCCCC");
}

void	green_palette(t_point *pt)
{
	if (pt->z < 0)
		pt->colour = ft_strdup("0x006600");
	else if (pt->z >= 0 && pt->z < 2)
		pt->colour = ft_strdup("0x009900");
	else if (pt->z >= 2 && pt->z < 4)
		pt->colour = ft_strdup("0x00CC00");
	else if (pt->z >= 4 && pt->z < 6)
		pt->colour = ft_strdup("0x00FF00");
	else if (pt->z >= 6 && pt->z < 8)
		pt->colour = ft_strdup("0x33FF99");
    else
		pt->colour = ft_strdup("0x99FFCC");
}

void	multi_palette(t_point *pt)
{
	if (pt->z < 0)
		pt->colour = ft_strdup("0xFFFFFF");
	else if (pt->z >= 0 && pt->z < 2)
		pt->colour = ft_strdup("0xFF99CC");
	else if (pt->z >= 2 && pt->z < 4)
		pt->colour = ft_strdup("0xB266FF");
	else if (pt->z >= 4 && pt->z < 6)
		pt->colour = ft_strdup("0x00CC66");
	else if (pt->z >= 6 && pt->z < 8)
		pt->colour = ft_strdup("0xFFFF00");
    else
		pt->colour = ft_strdup("0xFF0000");
}
