/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 12:39:01 by jwong             #+#    #+#             */
/*   Updated: 2018/04/26 17:27:41 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "libft/includes/libft.h"
#include "fdf.h"

void	error_system(void)
{
	char	*err;

	err = "Error: ";
	write(1, err, ft_strlen(err));
	err = strerror(errno);
	write(1, err, ft_strlen(err));
	write(1, "\n", 1);
	exit(-1);
}

void	error_program(char *msg)
{
	write(1, msg, ft_strlen(msg));
}

void    error_palette(void)
{
    char    *str;
    char    *usage;

    str = "error: wrong palette\n";
    usage = "Available palettes:\n1) blue\n2) green\n3) red\n4) multi\n";
    write(1, str, ft_strlen(str));
    write(1, usage, ft_strlen(usage));
    exit(-1);
}

int		error_handler(int err)
{
	if (err == ARG_ERR)
		error_program(ERRMSG_ARG);
	else if (err == FD_ERR || err == MALLOC_ERR)
		error_system();
	else if (err == FILE_ERR)
		error_program(ERRMSG_FILE);
	else if (err == FILE_EMPTY)
		error_program(ERRMSG_EMPTY_FILE);
    else if (err == PALETTE_ERR)
        error_palette();
	return (-1);
}
