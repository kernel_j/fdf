/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 15:50:52 by jwong             #+#    #+#             */
/*   Updated: 2018/04/25 15:18:31 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifdef __APPLE__
# define KEY_ESC 53
# define KEY_PLUS 69
# define KEY_MINUS 78
# define KEY_UP 126
# define KEY_DOWN 125
# define KEY_RIGHT 123
# define KEY_LEFT 124
# define KEY_PUP 116
# define KEY_PDOWN 121
#elif __unix__
# define KEY_ESC 65307
# define KEY_PLUS 65451
# define KEY_MINUS 65453
# define KEY_UP 65362
# define KEY_DOWN 65364
# define KEY_RIGHT 65363
# define KEY_LEFT 65361
# define KEY_PUP 65365
# define KEY_PDOWN 65366
#endif

#ifndef FDF_H
# define FDF_H

# include <math.h>
# include <stdlib.h>

# define TRUE 1
# define FALSE 0
# define MAX_WIN_LEN_X 1520
# define MAX_WIN_LEN_Y 1520
# define MAX_DIST 20
# define ALTITUDE 10
# define DEFAULT_COLOUR "0x7FFF00"
# define ISO_ANGLE_X 30 * (M_PI / 180)
# define ISO_ANGLE_Y (30 + 120) * (M_PI / 180)
# define ISO_ANGLE_Z (30 - 120) * (M_PI / 180)

# define ERRMSG_ARG "Error: wrong number of arguments\n"
# define ERRMSG_FILE "Error: file invalid\n"
# define ERRMSG_EMPTY_FILE "Error: file empty\n"

enum	e_palette
{
	BLUE,
	RED,
	GREEN,
	MULTI
};

enum	e_error
{
	ARG_ERR,
	FD_ERR,
	FILE_ERR,
	MALLOC_ERR,
	FILE_EMPTY,
    PALETTE_ERR
};

typedef struct	s_coord
{
	int			x0;
	int			x1;
	int			y0;
	int			y1;
	int			swap;
	int			s1;
	int			s2;
	int			dx;
	int			dy;
	int			p;
	int			x;
	int			y;
}				t_coord;

typedef	struct	s_point
{
	int			x;
	int			y;
	int			z;
	char		*colour;
}				t_point;

typedef struct	s_line
{
	size_t		len;
	t_point		*data;
}				t_line;

typedef struct	s_win
{
	void		*mlx;
	void		*win;
	void		*img_id;
	char		*img;
	double		dist;
	int			bpp;
	int			size_line;
	int			endian;
	int			offset_x;
	int			offset_y;
	int			alt;
	int			x;
	int			y;
	int			palette;
	t_line		*map;
}				t_win;

/*
** file_checker.c
*/
int				file_checker(char *file, t_win *win);

/*
** map.c
*/
t_line			*build_map(char *file, t_win *win);

/*
** algo.c
*/
int				iso_point_x(t_point *pt, int offset, double dist, int alt);
int				iso_point_y(t_point *pt, int offset, double dist, int alt);
void			bresenham(t_win *win, t_coord *coord, char *colour);

/*
** draw_map.c
*/
int				hex_to_rgb(const char *hex);
void			draw_map(t_win *win, t_line *map);
void			put_pixel(t_win *win, int x, int y, char *colour);

/*
** error.c
*/
int				error_handler(int err);

/*
** utils.c
*/
int				check_hex(char **line);
int				check_num(char **line, int *elem);

/*
** extract_data.c
*/
void			extract_data(char *line, t_point *pti, t_win *win);

/*
** window.c
*/
int				get_win_dimensions(t_win *win, t_line *map);
void			set_offset(t_win *win, t_line *map, int win_dist);

/*
** colours.c
*/
void			blue_palette(t_point *pt);
void			red_palette(t_point *pt);
void			green_palette(t_point *pt);
void			multi_palette(t_point *pt);

/*
** get_palette.c
*/
int				get_palette(char *palette, t_win *win);

/*
** fdf.c
*/
void			fdf(t_win *win, char *arg);

#endif
