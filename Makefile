# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jwong <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/07/19 15:25:47 by jwong             #+#    #+#              #
#    Updated: 2018/04/25 15:18:46 by jwong            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= fdf

SRC		= fdf.c				\
		  file_checker.c	\
		  algo.c			\
		  map.c				\
		  draw_map.c		\
		  error.c			\
		  utils.c			\
		  extract_data.c	\
		  window.c			\
		  colours.c			\
		  get_palette.c		\
		  main.c

CC		= gcc
OBJ		= $(SRC:.c=.o)

PLATFORM	:= $(shell uname)

ifeq	($(PLATFORM), Linux)
CFLAGS	= -Wall -Werror -Wextra -I libft/includes -I minilibx
else ifeq	($(PLATFORM), Darwin)
CFLAGS	= -Wall -Werror -Wextra -I libft/includes -I minilibx_macos
endif

all: $(NAME)

ifeq	($(PLATFORM), Linux)
$(NAME): $(OBJ)
		-make -C minilibx/
		make -C libft/
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L libft/ -L minilibx/ -lmlx -lXext -lX11 -lft -lm
		
%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		make clean -C minilibx/
		make clean -C libft/
		rm -f $(OBJ)

fclean: clean
		make fclean -C libft/
		rm -f $(NAME)

re: fclean all

else ifeq	($(PLATFORM), Darwin)
$(NAME): $(OBJ)
		make -C minilibx_macos/
		make -C libft/
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L libft/ -L minilibx_macos/ -framework OpenGL -framework AppKit -lmlx -lft -lm
		
%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		make -C minilibx_macos/ clean
		make -C libft/ clean
		rm -f $(OBJ)

fclean: clean
		make -C libft/ fclean
		rm -f $(NAME)

re: fclean all
endif
