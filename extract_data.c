/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extract_data.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 14:25:22 by jwong             #+#    #+#             */
/*   Updated: 2018/04/26 18:25:58 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fdf.h"

int		extract_number(char *line)
{
	char	*tmp;
	int		num;
	int		i;

    tmp = (char *)malloc(sizeof(char) * (ft_strlen(line) + 1));
    if (!tmp)
        error_handler(MALLOC_ERR);
	i = 0;
	while (*line && *line == ' ')
		line++;
	while (*line && *line != ' ' && *line != ',')
	{
		tmp[i] = *line;
		*line = ' ';
		line++;
		i++;
	}
	tmp[i] = '\0';
	if (ft_strlen(tmp) > 7)
	{
		error_handler(FILE_ERR);
		exit(-1);
	}
	num = ft_atoi(tmp);
	return (num);
}

char	*extract_colour(char *line)
{
	char	*hex;
	int		i;

	while (*line && *line == ' ')
		line++;
	if (*line != ',')
		return (NULL);
	hex = (char *)malloc(sizeof(char) * 9);
	if (!hex)
		error_handler(MALLOC_ERR);
	*line = ' ';
	line++;
	i = 0;
	while (*line && *line != ' ')
	{
		hex[i] = *line;
		*line = ' ';
		line++;
		i++;
	}
	hex[i] = '\0';
	return (hex);
}

void	extract_data(char *line, t_point *pt, t_win *win)
{
	pt->z = extract_number(line);
	pt->colour = extract_colour(line);
	if (!pt->colour)
	{
		free(pt->colour);
		if (win->palette == BLUE)
			blue_palette(pt);
        else if (win->palette == RED)
			red_palette(pt);
        else if (win->palette == GREEN)
			green_palette(pt);
		else
            multi_palette(pt);
	}
}
