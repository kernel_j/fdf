# README #
FDF is a program that renders 3D wireframes from files containing numbers that
represent x, y, z coordinates and hexadecimal for colour.

![alt text](https://bytebucket.org/kernel_j/fdf/raw/6311d92163f772caaa9f6ffb242abc7b0d98412c/img/fdf_france.png "Example_map")

### Install ###
Compile the program using the rules of the Makefile on the command line
```
make
```

To re-compile the project
```
make re
```

### Usage ###
FDF takes two arguements. The first is the map and the second is the colour
palette. There are four different colour palettes: blue, green, red, and
multi which is the default.

To run FDF
```
./fdf ./test_maps/42.fdf green
```

### Uninstall ###
To remove all object files
```
make clean
```

To remove all object files and the executable
```
make fclean
```

### MAPS ###
Each number in the map corresponds to a point in space:

* The horizontal position corresponds to the x-axis
* The vertical position corresponds to the y-axis
* The value corresponds to the altitude

```
cat test_maps/2-2.fdf

0 0
0 0
```
