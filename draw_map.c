/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 13:55:23 by jwong             #+#    #+#             */
/*   Updated: 2018/04/19 11:44:04 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "mlx.h"
#include "libft/includes/libft.h"
#include "fdf.h"

int		htoi(const char *hex)
{
	unsigned long	num;

	num = 0;
	while (*hex == '0' || *hex == 'x')
		hex++;
	while (*hex)
	{
		if (*hex >= '0' && *hex <= '9')
			num += *hex - '0';
		else if (*hex >= 'A' && *hex <= 'F')
			num += *hex - 'A' + 10;
		else if (*hex >= 'a' && *hex <= 'f')
			num += *hex - 'a' + 10;
		num *= 10;
		hex++;
	}
	num = num / 10;
	return ((int)num);
}

int		hex_to_rgb(const char *hex)
{
	char	*tmp;
	int		r;
	int		g;
	int		b;
	int		colour;

	tmp = ft_strsub(hex, 2, 2);
	r = htoi(tmp);
	free(tmp);
	tmp = ft_strsub(hex, 4, 2);
	g = htoi(tmp);
	free(tmp);
	tmp = ft_strsub(hex, 6, 2);
	b = htoi(tmp);
	free(tmp);
	colour = 0;
	colour += r << 16;
	colour += g << 8;
	colour += b;
	return (colour);
}

void	put_pixel(t_win *win, int x, int y, char *colour)
{
	int size;
	int rgb;

	if (x >= win->x || x < 0 || y >= win->y || y < 0)
		return ;
	rgb = hex_to_rgb(DEFAULT_COLOUR);
	if (colour)
		rgb = hex_to_rgb(colour);
	size = (y * win->size_line) + (x * win->bpp / 8);
	if (win->endian == 0)
	{
		win->img[size] = mlx_get_color_value(win->mlx, rgb);
		win->img[size + 1] = mlx_get_color_value(win->mlx, (rgb >> 8));
		win->img[size + 2] = mlx_get_color_value(win->mlx, (rgb >> 16));
	}
	else
	{
		win->img[size] = mlx_get_color_value(win->mlx, (rgb >> 16));
		win->img[size + 1] = mlx_get_color_value(win->mlx, (rgb >> 8));
		win->img[size + 2] = mlx_get_color_value(win->mlx, rgb);
	}
}

void	draw_line(t_win *win, t_point *p1, t_point *p2)
{
	t_coord	coord;

	ft_bzero(&coord, sizeof(coord));
	coord.x0 = iso_point_x(p1, win->offset_x, win->dist, win->alt);
	coord.y0 = iso_point_y(p1, win->offset_y, win->dist, win->alt);
	coord.x1 = iso_point_x(p2, win->offset_x, win->dist, win->alt);
	coord.y1 = iso_point_y(p2, win->offset_y, win->dist, win->alt);
	bresenham(win, &coord, p1->colour);
}

void	draw_map(t_win *win, t_line *map)
{
	size_t	i;
	size_t	j;

	i = 0;
	while (map[i].data)
	{
		j = 0;
		while (j < map[i].len)
		{
			if ((j + 1) < map[i].len && map[i + 1].data)
			{
				draw_line(win, &map[i].data[j], &map[i].data[j + 1]);
				draw_line(win, &map[i].data[j], &map[i + 1].data[j]);
			}
			else if ((j + 1) == map[i].len && map[i + 1].data)
				draw_line(win, &map[i].data[j], &map[i + 1].data[j]);
			else if ((j + 1) < map[i].len && !map[i + 1].data)
				draw_line(win, &map[i].data[j], &map[i].data[j + 1]);
			j++;
		}
		i++;
	}
}
