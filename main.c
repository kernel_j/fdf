/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 15:08:28 by jwong             #+#    #+#             */
/*   Updated: 2018/04/25 15:19:55 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "libft.h"
#include "fdf.h"

int		main(int argc, char **argv)
{
	t_win	win;
	int		ret;

	if (argc > 3 || argc < 2)
		return (error_handler(ARG_ERR));
	ft_bzero(&win, sizeof(win));
	if ((ret = file_checker(argv[1], &win)) == FALSE)
		exit(-1);
	if ((ret = get_palette(argv[2], &win)) == FALSE)
		error_handler(PALETTE_ERR);
	fdf(&win, argv[1]);
	return (0);
}
