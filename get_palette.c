/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_palette.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 15:01:20 by jwong             #+#    #+#             */
/*   Updated: 2018/04/25 15:01:23 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fdf.h"

char	*string_to_lower(const char *src, char *dest, int len)
{
	int	i;

	i = 0;
	while (src && i < len)
	{
		dest[i] = ft_tolower(*src);
		i++;
		src++;
	}
	dest[i] = '\0';
	return (dest);
}

int     set_palette(char *str, t_win *win)
{
    int ret;

    ret = TRUE;
	if (ft_strcmp("blue", str) == 0)
		win->palette = BLUE;
	else if (ft_strcmp("red", str) == 0)
		win->palette = RED;
	else if (ft_strcmp("green", str) == 0)
		win->palette = GREEN;
	else if (ft_strcmp("multi", str) == 0)
		win->palette = MULTI;
    else
        ret = FALSE;
    return (ret);
}

int		get_palette(char *palette, t_win *win)
{
	char	*str;
	int		ret;

	if (!palette)
	{
		win->palette = MULTI;
		return (TRUE);
	}
	str = (char *)malloc(sizeof(char) * ft_strlen(palette) + 1);
	if (!str)
	{
		free(str);
		error_handler(MALLOC_ERR);
	}
	string_to_lower(palette, str, ft_strlen(palette));
    ret = set_palette(str, win);
    free(str);
	return (ret);
}