/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 15:47:31 by jwong             #+#    #+#             */
/*   Updated: 2018/04/18 15:48:40 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "libft/includes/libft.h"
#include "mlx.h"
#include "fdf.h"

int		iso_point_x(t_point *pt, int offset, double dist, int alt)
{
	double	x_prime;
	double	y;
	double	x;

	y = pt->y * dist;
	x = pt->x * dist;
	x_prime = (x * cos(ISO_ANGLE_X))
				+ (y * cos(ISO_ANGLE_Y))
				+ (pt->z * alt * cos(ISO_ANGLE_Z))
				+ offset;
	return ((int)x_prime);
}

int		iso_point_y(t_point *pt, int offset, double dist, int alt)
{
	double	y_prime;
	double	y;
	double	x;

	y = pt->y * dist;
	x = pt->x * dist;
	y_prime = (x * sin(ISO_ANGLE_X))
				+ (y * sin(ISO_ANGLE_Y))
				+ (pt->z * alt * sin(ISO_ANGLE_Z))
				+ offset;
	return ((int)y_prime);
}

int		direction(int x)
{
	if (x > 0)
		return (1);
	else if (x < 0)
		return (-1);
	else
		return (0);
}

void	bresenham_init(t_win *win, t_coord *coord, char *colour)
{
	int	tmp;

	coord->dx = abs(coord->x1 - coord->x0);
	coord->dy = abs(coord->y1 - coord->y0);
	coord->s1 = direction(coord->x1 - coord->x0);
	coord->s2 = direction(coord->y1 - coord->y0);
	put_pixel(win, coord->x0, coord->y0, colour);
	if (coord->dy > coord->dx)
	{
		tmp = coord->dx;
		coord->dx = coord->dy;
		coord->dy = tmp;
		coord->swap = 1;
	}
	coord->p = 2 * coord->dy - coord->dx;
}

void	bresenham(t_win *win, t_coord *coord, char *colour)
{
	int	i;

	coord->x = coord->x0;
	coord->y = coord->y0;
	bresenham_init(win, coord, colour);
	i = 0;
	while (i < coord->dx)
	{
		put_pixel(win, coord->x, coord->y, colour);
		while (coord->p >= 0)
		{
			coord->p = coord->p - 2 * coord->dx;
			if (coord->swap)
				coord->x += coord->s1;
			else
				coord->y += coord->s2;
		}
		coord->p = coord->p + 2 * coord->dy;
		if (coord->swap)
			coord->y += coord->s2;
		else
			coord->x += coord->s1;
		put_pixel(win, coord->x, coord->y, colour);
		i++;
	}
}
