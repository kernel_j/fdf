/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 14:23:14 by jwong             #+#    #+#             */
/*   Updated: 2018/04/23 15:49:05 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include "libft/includes/libft.h"
#include "fdf.h"

t_line		*init_map(t_win *win)
{
	t_line	*map;

	map = (t_line *)malloc(sizeof(t_line) * (win->y + 1));
	if (!map)
		error_handler(MALLOC_ERR);
	return (map);
}

size_t		total_entries(char *s, char c)
{
	size_t	entry;
	size_t	i;

	entry = 0;
	i = 0;
	if (s[i] != c && s[i] != '\0')
		entry++;
	while (s[i])
	{
		if (s[i] == c)
		{
			if (i != 0 && s[i + 1] != '\0')
				if (s[i + 1] != c)
					entry++;
		}
		i++;
	}
	return (entry);
}

t_point		*store_data(char *line, size_t len, int i, t_win *win)
{
	t_point	*data;
	size_t	j;

	data = (t_point *)malloc(sizeof(t_point) * len);
	if (!data)
		error_handler(MALLOC_ERR);
	j = 0;
	while (j < len)
	{
		data[j].y = i;
		data[j].x = j;
		extract_data(line, &data[j], win);
		j++;
	}
	return (data);
}

t_line		*build_map(char *file, t_win *win)
{
	int		fd;
	char	*buff;
	t_line	*map;
	size_t	len;
	int		i;

	fd = open(file, O_RDONLY);
	if (fd == -1)
		error_handler(FD_ERR);
	buff = NULL;
	len = 0;
	i = 0;
	map = init_map(win);
	while (get_next_line(fd, &buff) > 0)
	{
		len = total_entries(buff, ' ');
		map[i].len = len;
		map[i].data = store_data(buff, len, i, win);
		free(buff);
		i++;
	}
	map[i].data = NULL;
	close(fd);
	return (map);
}
