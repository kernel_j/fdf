/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 16:21:47 by jwong             #+#    #+#             */
/*   Updated: 2018/04/25 15:19:13 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "mlx.h"
#include "libft.h"
#include "fdf.h"

int		free_map(t_line *map)
{
	size_t	i;
	size_t	j;
	t_point	*tmp;

	i = 0;
	while (map[i].data)
	{
		j = 0;
		while (j < map[i].len)
		{
			tmp = &map[i].data[j];
			free(tmp->colour);
			j++;
		}
		free(map[i].data);
		i++;
	}
	free(map[i].data);
	return (0);
}

void	clean_up(t_win *win)
{
	free_map(win->map);
	mlx_destroy_image(win->mlx, win->img_id);
	mlx_destroy_window(win->mlx, win->win);
	exit(0);
}

int		key_handler(int keycode, t_win *win)
{
	if (!win)
		return (-1);
	if (keycode == KEY_ESC)
		clean_up(win);
	if (keycode == KEY_PLUS)
		win->dist += 1;
	if (keycode == KEY_MINUS)
		win->dist -= 1;
	if (keycode == KEY_DOWN)
		win->offset_y += 5;
	if (keycode == KEY_UP)
		win->offset_y -= 5;
	if (keycode == KEY_RIGHT)
		win->offset_x += 5;
	if (keycode == KEY_LEFT)
		win->offset_x -= 5;
	if (keycode == KEY_PUP)
		win->alt += 1;
	if (keycode == KEY_PDOWN)
		win->alt -= 1;
	return (0);
}

int		redraw(t_win *win)
{
	mlx_destroy_image(win->mlx, win->img_id);
	win->img_id = mlx_new_image(win->mlx, win->x, win->y);
	win->img = mlx_get_data_addr(win->img_id, &win->bpp, &win->size_line,\
			&win->endian);
	draw_map(win, win->map);
	mlx_put_image_to_window(win->mlx, win->win, win->img_id, 0, 0);
	return (0);
}

void		fdf(t_win *win, char *arg)
{
	int		dist;

	win->mlx = mlx_init();
	win->alt = ALTITUDE;
	win->map = build_map(arg, win);
	dist = get_win_dimensions(win, win->map);
	set_offset(win, win->map, dist);
    if (win->x == 0 || win->y == 0)
    {
        error_handler(FILE_ERR);
        exit(-1);
    }
	win->win = mlx_new_window(win->mlx, win->x, win->y, "FDF");
	win->img_id = mlx_new_image(win->mlx, win->x, win->y);
	win->img = mlx_get_data_addr(win->img_id, &win->bpp, &win->size_line,\
			&win->endian);
	draw_map(win, win->map);
	mlx_key_hook(win->win, key_handler, win);
	mlx_put_image_to_window(win->mlx, win->win, win->img_id, 0, 0);
	mlx_loop_hook(win->mlx, redraw, win);
	mlx_loop(win->mlx);
}
